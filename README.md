# WILMER YASMANI CURRO GARATE 

## EJERCICIOS SQL
[1. Crear tablas (create table - describe - all_tables - drop table)](Solucionario/1.md)

[2. Ingresar registros (insert into- select)](Solucionario/2.md)

[3. Tipos de datos](Solucionario/3.md)

[4. Recuperación de algunos campos (select)](Solucionario/4.md)

[5. Recuperación de registros específicos (select . where)](Solucionario/5.md)

[6. Operadores relacionales](Solucionario/6.md)

[7. Borrar registros (delete)](Solucionario/7.md)

[8. Actualizar registros (update)](Solucionario/8.md)

[9. Comentarios](Solucionario/9.md)

[10. Valores nulos (null)](Solucionario/10.md)

[11 Operadores relacionales (is null)](Solucionario/11.md)

[12. Clave primaria (primary key)](Solucionario/12.md)

[13. Vaciar la tabla (truncate table)](Solucionario/13.md)

[14. Tipos de datos alfanuméricos](Solucionario/14.md)

[15. Tipos de datos numéricos](Solucionario/15.md)

[16. Ingresar algunos campos](Solucionario/16.md)

[17. Valores por defecto (default)](Solucionario/17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](Solucionario/18.md)

[19.  Alias (encabezados de columnas)](Solucionario/19.md)

[20. Funciones string# 20. Funciones string](Solucionario/20.md)

[21. Funciones matemáticas](Solucionario/21.md)

[22. Funciones de fechas y horas](Solucionario/22.md)

[23. Ordenar registros (order by)](Solucionario/23.md)

[24. Operadores lógicos (and - or - not)](Solucionario/24.md)

[25. Otros operadores relacionales (between)](Solucionario/25.md)

[26. Otros operadores relacionales (in)](Solucionario/26.md)

[27. Búsqueda de patrones (like - not like)](Solucionario/27.md)

[28. Contar registros (count)](Solucionario/28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](Solucionario/29.md)

[30. Agrupar registros (group by)](Solucionario/30.md)

[31. Seleccionar grupos (Having)](Solucionario/31.md)

[32. Registros duplicados (Distinct)](Solucionario/32.md)

[33. Clave primaria compuesta](Solucionario/33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](Solucionario/34.md)

[36. Integridad de datos](Solucionario/35.md)

[37. Restricción primary key](Solucionario/36.md)

[37. Actualizar registros (update)](Solucionario/37.md)

[38. Restricción unique)](Solucionario/38.md)

[39. Restriccion check](Solucionario/39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](Solucionario/40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](Solucionario/41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](Solucionario/42.md)

[43. Indices](Solucionario/43.md)

[44. Indices (Crear . Información)](Solucionario/44.md)

[45. Indices (eliminar)](Solucionario/45.md)

[46. Varias tablas (join)](Solucionario/46.md)

[47. Combinación interna (join)](Solucionario/47.md)

[48. Combinación externa izquierda (left join)](Solucionario/48.md)

[49. Combinación externa derecha (right join)](Solucionario/49.md)

[50. Combinación externa completa (full join)](Solucionario/50.md)

[51. Combinaciones cruzadas (cross)](Solucionario/51.md)

[52. Autocombinación](Solucionario/52.md)

[53. Combinaciones y funciones de agrupamiento	](Solucionario/53.md)

[54. Combinar más de 2 tablas](Solucionario/54.md)

[55. Otros tipos de combinaciones)](Solucionario/55.md)

[56. Clave foránea](Solucionario/56.md)

[57. Restricciones (foreign key)](Solucionario/57.md)

[58. Restricciones foreign key en la misma tabla  ](Solucionario/58.md)

[59. Restricciones foreign key (eliminación)](Solucionario/59.md)

[60. Restricciones foreign key deshabilitar y validar](Solucionario/60.md)

[61. Restricciones foreign key (acciones)](Solucionario/61.md)

[62. Información de user_constraints](Solucionario/62.md)

[63. Restricciones al crear la tabla](Solucionario/63.md)

[64. - Unión](Solucionario/64.md)

[65. Intersección](Solucionario/65.md)

[66. Minus](Solucionario/66.md)

[67. Agregar campos (alter table-add)](Solucionario/67.md)

[68. Modificar campos (alter table - modify)    ](Solucionario/68.md)

[69. Eliminar campos (alter table - drop)](Solucionario/69.md)

[70.Agregar campos y restricciones (alter table)](Solucionario/70.md)

[71. Subconsultas](Solucionario/71.md)

[72.Subconsultas como expresion](Solucionario/72.md)

[73. Subconsultas con in](Solucionario/73.md)

[74. Subconsultas any- some - all](Solucionario/74.md)

[75. Subconsultas correlacionadas](Solucionario/75.md)

[76. Exists y No Exists](Solucionario/76.md)

[77. Subconsulta simil autocombinacion](Solucionario/77.md)

[78. Subconsulta conupdate y delete](Solucionario/78.md)

[79. Subconsulta e insert](Solucionario/79.md)

[80. Crear tabla a partir de otra (create table-select)](Solucionario/80.md)

[81. Vistas (create view)](Solucionario/81.md)

[82. Vistas (información)](Solucionario/82.md)

[83. Vistas eliminar (drop view)](Solucionario/83.md)

[84. Vistas (modificar datos a través de ella)](Solucionario/84.md)

[85. Vistas (with read only)](Solucionario/85.md)

[86.  Vistas modificar (create or replace view)](Solucionario/86.md)

[87. Vistas (with check option)](Solucionario/87.md)

[88. Vistas (otras consideraciones: force)](Solucionario/88.md)

[89.  Vistas materializadas (materialized view)](Solucionario/89.md)

[90.  Procedimientos almacenados](Solucionario/90.md)

[91.  Procedimientos Almacenados (crear- ejecutar)](Solucionario/91.md)

[92. Procedimientos Almacenados (eliminar)](Solucionario/92.md)

[93. Procedimientos almacenados (parámetros de entrada)](Solucionario/93.md)

[94. Procedimientos almacenados (variables)](Solucionario/94.md)

[95. Procedimientos Almacenados (informacion)](Solucionario/95.md)

[96. Funciones](Solucionario/96.md)

[97. Control de flujo (if)](Solucionario/97.md)

[98. Control de flujo (case)](Solucionario/98.md)

[99. Control de flujo (loop)](Solucionario/99.md)

[100. Control de flujo (for)](Solucionario/100.md)

[101. Control de flujo (bucle while)](Solucionario/101.md)

[102. Disparador (gatillo)](Solucionario/102.md)

[103. Disparador (información)](Solucionario/103.md)

[104. Disparador de insercion a nivel de sentencia](Solucionario/104.md)

[105. Disparador de inserción a nivel de fila (insertar gatillo para cada fila)](Solucionario/105.md)

[106.Disparador de borrado (nivel de sentencia y de fila)](Solucionario/106.md)

[107. Disparador de actualización a nivel de sentencia (update trigger)](Solucionario/107.md)

[108.  Disparador de actualización a nivel de fila (update trigger)](Solucionario/108.md)

[109. Disparador de actualización - lista de campos (update trigger)](Solucionario/109.md)

[110.  Disparador de multiples eventos](Solucionario/110.md)

[111. Disparador (old y new)](Solucionario/111.md)

[112. Disparador condiciones (when)](Solucionario/112.md)

[113. Disparador de actualizacion - campos (updating)](Solucionario/113.md)

[114. Disparadores (habilitar y deshabilitar)](Solucionario/114.md)

[115. Disparador (eliminar)](Solucionario/115.md)

[116. Errores definidos por el usuario en trigger](Solucionario/116.md)

[117. Seguridad y acceso a Oracle](Solucionario/117.md)

[118. Usuarios (crear)](Solucionario/118.md)

[119. 	Permiso de conexión](Solucionario/119.md)

[120. Privilegios del sistema (conceder)](Solucionario/120.md)

[121. Privilegios del sistema (with admin option)](Solucionario/121.md)

[122. Modelado de base de datos](Solucionario/122.md)



